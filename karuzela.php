<?php
/* Karuzela bez użycia JavaScript, wyświetlająca pobrane 'Kupili rowniez' 	*/
/* Autor: Michał Gałęziowski 												*/

/* Domyślny numer aukcji, jesli nei można pobrać z linku */
	$matches = '181869643012';

/* Pobieramy numer aukcji z linku */
	if(isset($_SERVER['REQUEST_URI'])){
		$auction_id = $_SERVER['REQUEST_URI'];
		preg_match('/\\d{12}/', $auction_id, $matches);
	}

	$host = '/itm/';
/* Pobranie propozycji dla konkretnej aukcji */
	require_once('databaseConnection.class.php');
	$conn = new databaseConnection;
	$results = $conn->runQuery('call app_othersbought.show_proposals('.$matches[0].')');

/* Przygotowanie rozpiski ktore propozycje maja byc wyswietlone w karuzeli 													*/
/* carousel - jest to numer 'wagonika', zawiera tablice z numerami aukcji, ustawionymi tak, aby przesuwanie pozwalalo		*/
/* zachować zasade działania karuzeli 																						*/
	$all = array(
					'carousel-1' => array(5,0,1),
					'carousel-2' => array(0,1,2),
					'carousel-3' => array(1,2,3),
					'carousel-4' => array(2,3,4),
					'carousel-5' => array(3,4,5),
					'carousel-6' => array(4,5,0)
				);

?>

<html>
	<head>
		<link rel="Stylesheet" type="text/css" href="style.css" />
	</head>
<body>

<div id='container'>
	<div class="carousel">
	   	<div class="carousel-inner">        	

		<?php

			$a = 'checked="checked"';
			foreach ($all as $key => $innerArray) {
				echo '<input class="carousel-open" type="radio" id="'. $key . '" name="carousel" aria-hidden="true" hidden="" '.$a.'>
				 	<div class="carousel-item">
					    <div class=\'item_to_show_left\' id=\'catB\' >
					       	<a class="not-active" href="'. $host . $results[$innerArray[0]]['auction_id'] .'">
					          	<div class=\'polaroid\'>
					           		<img src='.$results[$innerArray[0]]['pictureurl'].' />
					           		<div class=\'auction_title\'>
					           			'.$results[$innerArray[0]]['title'] . '<br />
					               	</div>
					            	<div class=\'price\'>
					            		' . $results[$innerArray[0]]['price'].' ' . $results[$innerArray[0]]['currency'] .'
					            	</div> 
					           	</div>	
					       	</a>
					      	<div class=\'mirror\'></div>
					    </div>
					   
				        <div class=\'item_to_show_inside\'></div>

					    <div class=\'item_to_show_center\' id=\'catA\' >
					       	<a class="not-active" href=" '. $host . $results[$innerArray[1]]['auction_id'] .' ">
					           	<div class=\'polaroid\'  >
					           		<img src='.$results[$innerArray[1]]['pictureurl'].' />			           		
					       	   		<div class=\'auction_title\' >
					           			' . $results[$innerArray[1]]['title'] . '<br />
					           		</div>
					            	<div class=\'price\'>
					            		' . $results[$innerArray[1]]['price'].' ' . $results[$innerArray[1]]['currency'] . '
					               	</div> 
					           	</div>		
					        </a>
					        <div class=\'mirror\'></div>
					    </div>


				        <div class=\'item_to_show_inside\' ></div>
					        
					    <div class=\'item_to_show_right\' >
					      	<a class="not-active" href="' . $host . $results[$innerArray[2]]['auction_id'] . '">
					           	<div class=\'polaroid\'>
					           		<img src='.$results[$innerArray[2]]['pictureurl'].' />
					           		<div class=\'auction_title\'>
					           			' . $results[$innerArray[2]]['title'] . '<br />
					           		</div>
					            	<div class=\'price\'>
					            	    ' . $results[$innerArray[2]]['price'].' ' . $results[$innerArray[2]]['currency'] .'
					            	</div> 
					           	</div>	
					       	</a>
					    </div>
					</div> ';
					$a = '';
			};

			echo '
		        <label for="carousel-3" class="carousel-control prev control-4"><img src=\'img/left.png\' width="30%" height="10%" /></label>
		        <label for="carousel-3" class="carousel-control next control-2"><img src=\'img/right.png\' width="30%" height="10%" /></label>
		        
		        <label for="carousel-1" class="carousel-control prev control-2"><img src=\'img/left.png\' width="30%" height="10%" /></label>
		        <label for="carousel-1" class="carousel-control next control-6"><img src=\'img/right.png\' width="30%" height="10%" /></label>
		        
		        <label for="carousel-2" class="carousel-control prev control-3"><img src=\'img/left.png\' width="30%" height="10%" /></label>
		        <label for="carousel-2" class="carousel-control next control-1"><img src=\'img/right.png\' width="30%" height="10%" /></label>

		        <label for="carousel-4" class="carousel-control prev control-5"><img src=\'img/left.png\' width="30%" height="10%" /></label>
		        <label for="carousel-4" class="carousel-control next control-3"><img src=\'img/right.png\' width="30%" height="10%" /></label>

		        <label for="carousel-5" class="carousel-control prev control-6"><img src=\'img/left.png\' width="30%" height="10%" /></label>
		        <label for="carousel-5" class="carousel-control next control-4"><img src=\'img/right.png\' width="30%" height="10%" /></label>

		        <label for="carousel-6" class="carousel-control prev control-1"><img src=\'img/left.png\' width="30%" height="10%" /></label>
		        <label for="carousel-6" class="carousel-control next control-5"><img src=\'img/right.png\' width="30%" height="10%" /></label>
	        	
	    </div>
	</div>
</div>';

		?>

</body>
</html>